package com.personal.postgresql.mappers;

import com.personal.postgresql.dto.MaestroValorDTO;
import com.personal.postgresql.dto.UsuarioDTO;
import com.personal.postgresql.entities.MaestroValor;
import com.personal.postgresql.entities.Usuario;

public class UsuarioMapper {
	private final MaestroValorMapper maestroValorMapper = new MaestroValorMapper();
	
	public UsuarioDTO obtenerUsrDTO(Usuario usrACastear) {
		UsuarioDTO usrDtoARetornar = new UsuarioDTO();
		usrDtoARetornar.setNombres(usrACastear.getNombres());
		usrDtoARetornar.setApellidos(usrACastear.getApellidos());
		usrDtoARetornar.setDireccion(usrACastear.getDireccion());
		usrDtoARetornar.setDocumento(usrACastear.getDocumento());
		usrDtoARetornar.setEmail(usrACastear.getEmail());
		usrDtoARetornar.setFechaNacimiento(usrACastear.getFechaNacimiento());
		usrDtoARetornar.setTelefono(usrACastear.getTelefono());
		usrDtoARetornar.setContrasena(usrACastear.getContrasena());
//		usrDtoARetornar.setTipoDocumento(usrACastear.getTipoDocumento());
//		usrDtoARetornar.setTipoSangre(usrACastear.getTipoSangre());
		// usrDtoARetornar.setIdDatoBiometrico(usrACastear.getIdDatoBiometrico());
		// usrDtoARetornar.setIdCuenta(usrACastear.getIdCuenta());

		return usrDtoARetornar;
	}
	
	public Usuario obtenerUsr(UsuarioDTO usrACastear) {
		Usuario usrARetornar = new Usuario();
		MaestroValor tipoDoc = this.maestroValorMapper.obtenerMaestroValor(usrACastear.getTipoDocumento());
		MaestroValor tipoSangre = this.maestroValorMapper.obtenerMaestroValor(usrACastear.getTipoSangre());

		tipoDoc.setId(usrACastear.getTipoDocumento().getId());
		tipoSangre.setId(usrACastear.getTipoSangre().getId());

		usrARetornar.setNombres(usrACastear.getNombres());
		usrARetornar.setApellidos(usrACastear.getApellidos());
		usrARetornar.setDireccion(usrACastear.getDireccion());
		usrARetornar.setDocumento(usrACastear.getDocumento());
		usrARetornar.setEmail(usrACastear.getEmail());
		usrARetornar.setFechaNacimiento(usrACastear.getFechaNacimiento());
		usrARetornar.setTelefono(usrACastear.getTelefono());
		usrARetornar.setContrasena(usrACastear.getContrasena());
		usrARetornar.setTipoDocumento(tipoDoc);
		usrARetornar.setTipoSangre(tipoSangre);
		// usrDtoARetornar.setIdDatoBiometrico(usrNuevo.getIdDatoBiometrico());
		// usrDtoARetornar.setIdCuenta(usrNuevo.getIdCuenta());

		return usrARetornar;
	}

}
