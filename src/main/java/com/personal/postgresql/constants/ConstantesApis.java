package com.personal.postgresql.constants;

public class ConstantesApis {

	public static final String GEOLOCALIZACION_API = "geo/api";
	public static final String GEOLOCALIZACION_API_OBTENER = "/obtener";
	public static final String GEOLOCALIZACION_API_GUARDAR = "/guardar";
	
	public static final String VEHICULO_API = "vehiculo/api";
	public static final String VEHICULO_API_GUARDAR = "/guardar-o-actualizar";
	public static final String VEHICULO_API_BORRAR = "/borrar";
	
	public static final String USUARIO_API = "usr/api";
	public static final String USUARIO_API_OBTENER = "/obtenerUsuario";
	public static final String USUARIO_API_GUARDAR = "/guardarUsuario";
	public static final String USUARIO_API_ACTUALIZAR = "/actualizarUsuario";
	public static final String USUARIO_API_ELIMINAR = "/eliminarUsuario";
	
}
