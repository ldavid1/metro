package com.personal.postgresql.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.personal.postgresql.entities.Vehiculo;

@Repository
public interface VehiculoDao extends CrudRepository<Vehiculo, Long> {

}
